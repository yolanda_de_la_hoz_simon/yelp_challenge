# Yelp Dataset Challenge
   
This dataset is provided by Yelp, a service that allows users to review businesses and check other users reviews. They provide a subset of their data in a challenge (which is in its 6th round) to promote the development of innovative visual analytic tools. This dataset contains geolocalized information about businesses, users reviews and scores, etc. Many different analyses could be carried out on these data and they support many of the encoding options seen in class (cartographic arrangement, filtering of data, etc.).

The data can be downloaded from http://www.yelp.com/dataset_challenge.


## Contact information

Yolanda de la Hoz Simón. yolanda93h@gmail.com